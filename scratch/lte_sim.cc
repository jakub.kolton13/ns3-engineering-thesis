#include <ns3/core-module.h>
#include <ns3/network-module.h>
#include <ns3/mobility-module.h>
#include <ns3/lte-module.h>
#include <ns3/config-store.h>

using namespace ns3;

int main(int argc, char *argv[]){
/*	CommandLine cmd (__FILE__);
	cmd.Parse(argc, argv);
	ConfigStore inputConfig;
	inputConfig.ConfigureDefaults();

	cmd.Parse(argc, argv);\

	Ptr<LteHelper> lteHelper = CreateObject<LteHelper>();
	lteHelper->SetSchedulerType("ns3::TtaFfMacScheduler");
	lteHelper->SetFadingModel("ns3::TraceFadingLossModel");

	NodeContainer enbNodes;
	enbNodes.Create(2);
	NodeContainer ueNodes;
	ueNodes1.Create(1);
	ueNodes2.Create(1);

	MobilityHelper ue1mobility;
	ue1mobility.SetPositionAllocator("ns3::UniformDiscPositionAllocator",
					"X", DoubleValue(0.0),
					"Y", DoubleValue(0.0),
					"rho", DoubleValue(radius));
	ue1mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	ue1mobility.Install(ueNodes1);

	MobilityHelper ue2mobility;
	ue2mobility.SetPositionAllocator("ns3::UniformDiscPositionAllocator",
					"X", DoubleValue(enbDist),
					"Y", DoubleValue(0.0),
					"rho", DoubleValue(radius));
	ue2mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	mobility.Install(ueNodes2);

	NetDeviceContainer enbDevs;
	enbDevs = lteHelper->InstallEnbDevice(enbNodes);

	NetDeviceContainer ueDevs1;
	ueDevs1 = lteHelper->InstallUeDevice(ueNodes1);

	NetDeviceContainer ueDevs2;
	ueDevs2 = lteHelper->InstallUeDevice(ueNodes1);

	lteHelper->Attach(ueDevs1, enbDevs.Get(0));

	enum EpsBearer::Qci q = EpsBearer::GBR_CONV_VOICE;
	EpsBearer bearer(q);
	lteHelper->ActivateDataRadioBearer(ueDevs, bearer);

	lteHelper->EnablePhyTraces();
	lteHelper->EnableMacTraces();
	lteHelper->EnableRlcTraces();
	lteHelper->EnablePdcpTraces();

	lteHelper->SetFadingModelAttribute("TraceFilename", StringValue("src/lte/model/fading-traces/fading_trace_EPA_3kmph.fad"));
	lteHelper->SetFadingModelAttribute("TraceLength", TimeValue(Seconds(10.0)));
	lteHelper->SetFadingModelAttribute("SamplesNum", UintegerValue(10000));
	lteHelper->SetFadingModelAttribute("WindowSize", TimeValue(Seconds(0.5)));
	lteHelper->SetFadingModelAttribute("RbNum", UintegerValue(100));

	Simulator::Stop(Seconds(0.005));

	Simulator::Run();

	Simulator::Destroy();
*/	return 0;
}
